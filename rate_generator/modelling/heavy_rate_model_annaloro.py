

import math
import numpy as np


mA = 6.635914511e-26 #kg, mass of neutral argon
pi = np.pi
sigma_0 = 10e-20
a1 = 0.39534
a2 = 0.3546
h = 6.62607e-34 #m^2 kg / s, planck's constant
kb = 1.38064852e-23 #J/K, Boltzmann Constant
me = 9.10938356e-31 #kg, electron mass
mu = mA*me/(mA + me) #kg, reduced mass


def get_rate_coef(reaction, T):
    kf = universal_rate(reaction, T)
    kr = get_kr_from_kf(reaction, T, kf)
    Kc = kf/kr
    return kf, Kc

def universal_rate(reaction, T):
    """
        Annaloro only presents a single explicit formula for atomic impact excitation
    """
    return sqrt_term(T) * sigma_0 * a1 * pow(xmin(reaction, T), a2) * np.exp(-xmin(reaction, T))

def get_kr_from_kf(reaction, T, kf):
    if reaction.transitionType == 'ion':
        return kf * (reaction.lowerData.g/reaction.upperData.g) * 0.5 * (pow(((pow(h, 2))/(2*pi*me*kb*T)), 1.5)) * np.exp((reaction.upperData.E - reaction.lowerData.E)/(kb*T))
    else: 
        return kf * (reaction.lowerData.g/reaction.upperData.g) * np.exp((reaction.upperData.E - reaction.lowerData.E)/(kb*T))

def xmin(reaction, T):
    return (reaction.upperData.E - reaction.lowerData.E)/(kb*T)

def sqrt_term(T):
    return math.sqrt(8*kb*T/(pi*mu))