"""
    Author: Brad Semple
    Date: 2020 Mar 14

    Creates a csv file with the rate fitting parameters for heavy particle impact excitation/ionisation for neutral argon

    output csv file columns take the form:
    i, j, A, n, E, G1, G2, G3, G4, G5
    Which represent
    i - lower energy level
    j - upper energy level
    A, n, E - fitting parameters for forward reaction rate coefficient in equation, kf = A * T^n * exp[-E/T]
    G1, G2, G3, G4, G5 - fitting parameters for equilibrium coefficient in equation, Kc = exp[G1*(T/1e4) + G2 + G3*Ln(1e4/T) + G4*1e4/T + G5*(1e4/T)^2]
"""
from modelling.electronic_reaction_tools import get_heavy_reaction_list
import pandas as pd
import numpy as np

reactionList = get_heavy_reaction_list()

columns = ['i', 'j', 'A', 'n', 'E', 'G1', 'G2', 'G3', 'G4', 'G5']
out_data = np.zeros((len(reactionList), 10))
for i, react in enumerate(reactionList):
    out_data[i] = [react.i, react.j, react.forwardReactionParams.A, react.forwardReactionParams.n, react.forwardReactionParams.E,
                   react.reverseReactionParams.G1, react.reverseReactionParams.G2, react.reverseReactionParams.G3, react.reverseReactionParams.G4, react.reverseReactionParams.G5]
df = pd.DataFrame(out_data, columns=columns)
df.to_csv('../data/heavy_rate_fit_data.csv', index=False)