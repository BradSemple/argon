import pandas as pd
import numpy as np
from modelling.reaction_class import Reaction
import matplotlib.pyplot as plt

def get_electronic_reaction_list():
    data = pd.read_csv('../data/levels.csv')

    reaction_list = list()
    for i in range(len(data)-1):
        for j in range(i+1, len(data)):
            lower_index = data.iloc[i][0]
            upper_index = data.iloc[j][0]
            reaction_list.append(Reaction(lower_index, upper_index, 'e'))
    data.set_index('index', inplace=True)
    for react in reaction_list:
        react.fill_data(data)
        react.get_fit_parameters()
        print("Done with reaction {} - {}".format(react.i, react.j))
    return reaction_list

def get_heavy_reaction_list():
    data = pd.read_csv('../data/levels.csv')

    reaction_list = list()
    for i in range(len(data)-1):
        for j in range(i+1, len(data)):
            lower_index = data.iloc[i][0]
            upper_index = data.iloc[j][0]
            reaction_list.append(Reaction(lower_index, upper_index, 'A'))
    data.set_index('index', inplace=True)
    for react in reaction_list:
        react.fill_data(data)
        react.get_fit_parameters()
        print("Done with reaction {} - {}".format(react.i, react.j))
    return reaction_list

def plot_kf_from_model(reaction):
    Tlist = np.linspace(2000, 30000, 100)
    kflist = np.zeros(100)
    for i, T in enumerate(Tlist):
        kflist[i] = reaction.get_rate_coef(T)[0]
    plt.plot(Tlist, kflist, label = "kf from model for transition {} to {}".format(reaction.i, reaction.j))
    plt.yscale('log')
    plt.ylabel(r'$m^{3} s^{-1}$')
    plt.xlabel(r'$K$')
    plt.title('Reaction Rate Coefficient')
    plt.legend()


def plot_kf_from_fit(reaction):
    Tlist = np.linspace(2000, 30000, 100)
    kflist = np.zeros(100)
    A, n, E = reaction.forwardReactionParams
    for i, T in enumerate(Tlist):
        kflist[i] = A * pow(T, n) * np.exp(-E/T)
    plt.plot(Tlist, kflist, label = "kf from fit for transition {} to {}".format(reaction.i, reaction.j))
    plt.yscale('log')
    plt.ylabel(r'$m^{3} s^{-1}$')
    plt.xlabel(r'$K$')
    plt.title('Reaction Rate Coefficient')
    plt.legend()


def plot_Kc_from_model(reaction):
    Tlist = np.linspace(2000, 30000, 100)
    Kclist = np.zeros(100)
    for i, T in enumerate(Tlist):
        Kclist[i] = reaction.get_rate_coef(T)[1]
    plt.plot(Tlist, Kclist, label = "Kc from model for transition {} to {}".format(reaction.i, reaction.j))
    plt.yscale('log')
    plt.ylabel(r'$m^{3} s^{-1}$')
    plt.xlabel(r'$K$')
    plt.title('Equilibrium Coefficient')
    plt.legend()


def plot_Kc_from_fit(reaction):
    Tlist = np.linspace(2000, 30000, 100)
    zlist = 1e4/Tlist
    Kclist = np.zeros(100)
    G1, G2, G3, G4, G5 = reaction.reverseReactionParams
    for i, z in enumerate(zlist):
        Kclist[i] = np.exp(G1/z + G2 + G3*np.log(z) + G4*z + pow(G5, 2))
    plt.plot(Tlist, Kclist, label = "Kc from fit for transition {} to {}".format(reaction.i, reaction.j))
    plt.yscale('log')
    plt.ylabel(r'$m^{3} s^{-1}$')
    plt.xlabel(r'$K$')
    plt.title('Equilibrium Coefficient')
    plt.legend()

