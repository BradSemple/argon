#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 14:33:22 2020

@author: brad
"""

from scipy.integrate import quad
from math import sqrt, factorial
import numpy as np

pi = np.pi
a0 = 5.29e-11 #m, First Bohr radius
Eh = 2.18e-18 #J, Ionisation energy of Hydrogen
kb = 1.38064852e-23 #J/K, Boltzmann Constant
me = 9.10938356e-31 #kg, electron mass
alpha_a = 1
alpha_p = 0.05
alpha_s = 0.1
alpha_ion = 0.67
beta_A = 1
beta_ion = 1
h = 6.62607e-34 #m^2 kg / s, planck's constant
c = 2.99792458e8 #m/s speed of light in a vacuum
Ar_ion_energy = 127109.842 * h * c*100 #J, ionisation energy of argon (multiply by 100 to convert wave number in cm^-1 to m^-1)


def get_rate_coef(reaction, T):
    if reaction.transitionType == 'ion':
        kf = ion_rate(reaction, T)
    elif reaction.transitionType == 'A':
        kf = A_rate(reaction, T)
    elif reaction.transitionType == 'P':
        kf = P_rate(reaction, T)
    elif reaction.transitionType == 'S':
        kf = S_rate(reaction, T)
    else: print("Error: Unrecognised transition type: {}".format(reaction.transitionType))
    kr = get_kr_from_kf(reaction, T, kf)
    Kc = kf/kr
    return kf, Kc

def A_rate(reaction, T):
    a_term = a(reaction, T)

    return thermal_electron_mean_velocity(T) * 4 * pi * pow(a0, 2) * pow(a_term, 2) * alpha_a * ion_squared_term(reaction) * I2(reaction, T)

def P_rate(reaction, T):
    a_term = a(reaction, T)
    return thermal_electron_mean_velocity(T) * 4 * pi * pow(a0, 2) * pow(a_term, 2) * alpha_p * I1(reaction, T)

def S_rate(reaction, T):
    a_term = a(reaction, T)
    return thermal_electron_mean_velocity(T) * 4 * pi * pow(a0, 2) * pow(a_term, 2) * alpha_s * I3(reaction, T)

def ion_rate(reaction, T):
    a_term = a(reaction, T)
    return thermal_electron_mean_velocity(T) * 4 * pi * pow(a0, 2) * pow(a_term, 2) * alpha_ion * ion_squared_term(reaction) * I2(reaction, T)

def get_kr_from_kf(reaction, T, kf):
    if reaction.transitionType == 'ion':
        return kf * (reaction.lowerData.g/reaction.upperData.g) * 0.5 * (pow(((pow(h, 2))/(2*pi*me*kb*T)), 1.5)) * np.exp((reaction.upperData.E - reaction.lowerData.E)/(kb*T))
    else: 
        return kf * (reaction.lowerData.g/reaction.upperData.g) * np.exp((reaction.upperData.E - reaction.lowerData.E)/(kb*T))


# The following functions are defined to segment and simplify the long calculation of the forward reaction rates
def thermal_electron_mean_velocity(T):
    return sqrt((8*kb*T)/(pi*me))

def a(reaction, T):
    return (reaction.upperData.E - reaction.lowerData.E)/(kb*T)

def ion_squared_term(reaction):
    return pow((Eh/(reaction.upperData.E - reaction.lowerData.E)), 2)

def I1(reaction, T):
    a_term = a(reaction, T)
    return np.exp(-a_term)/a_term - exp_int(1, a_term)

def I2(reaction, T):
    a_term = a(reaction, T)
    beta = beta_A
    if reaction.transitionType == 'ion':
        beta = beta_ion
    return I1(reaction, T) * np.log(1.25 * beta) + exp_int(1, a_term)/a_term - gen_exp_int(2, a_term)

def I3(reaction, T):
    a_term = a(reaction, T)
    return exp_int(2, a_term) - exp_int(4, a_term)


#The following functions are defined to calculate the exponential integrals and generalised exponential integrals
def exp_int_arg(x, n, a):
    """
        Argument inside the integral for the exponential integral of order 'n' evaluated for 'a'
    """
    return (pow(x, -n))*(np.exp(-a*x))

def exp_int(n, a):
    """
        Returns the value for the exponential integral of order 'n' evaluated at 'a'
    """
    return quad(exp_int_arg, 1, np.inf, (n, a))[0]

def gen_exp_int_arg(x, n, a):
    """
        Argument inside the integral for the generalised exponential integral of order 'n' evaluated for 'a'
    """
    return np.exp(-a*x)*pow(np.log(x), (n-1))*(1.0/x)

def gen_exp_int(n, a):
    return (1/factorial(n-1))*quad(gen_exp_int_arg, 1, np.inf, (n,a))[0]


