"""
    Author: Brad Semple
    Date: 2020-Mar-13

    This module creates fitting parameters to curve fit a reaction to a
    modified arrhenius form such that k = A * T^n * exp[-E/T]
    For calculating 

    This is done by the following steps
    1. Create an array of temperatures spanning the required temperature range in regular intervals
    2. Create array of forward reaction rate coefficients and equilibrium coefficients for each temperature
    3. Create 
"""
import numpy as np
from fitting.least_squares_fit import lso

def get_fit_parameters(reaction):
    #Define some parameters for function fitting
    lower_temp_limit = 2000 #K
    upper_temp_limit = 30000 #K
    n_temps = int((upper_temp_limit - lower_temp_limit)/100)

    #Define temperatures and storage space for each value of kf and Kc
    Tlist = np.linspace(lower_temp_limit, upper_temp_limit, n_temps)
    kf_list = np.zeros(n_temps)
    Kc_list = np.zeros(n_temps)

    #Define basis function set for forward reaction such that:
    # Ln(kf) = Ln(A) + n * Ln(T) + E * (-1/T)
    kf_phi_1 = lambda T: 1
    kf_phi_2 = lambda T: np.log(T)
    kf_phi_3 = lambda T: -1.0/T
    kf_phi = [kf_phi_1, kf_phi_2, kf_phi_3]

    #Define basis function set for equilibrium coefficient such that:
    # Ln(Kc) = G1 * (T/1e4) + G2 + G3 * Ln(1e4/T) + G4 * 1e4/T + G5 * (1e4/T)^2
    Kc_phi_1 = lambda T: T/1e4
    Kc_phi_2 = lambda T: 1
    Kc_phi_3 = lambda T: np.log(1e4/T)
    Kc_phi_4 = lambda T: 1e4/T
    Kc_phi_5 = lambda T: pow((1e4/T), 2)
    Kc_phi = [Kc_phi_1, Kc_phi_2, Kc_phi_3, Kc_phi_4, Kc_phi_5]

    #Create list of function values, and take their log
    for i in range(n_temps):
        kf_list[i], Kc_list[i] = reaction.get_rate_coef(Tlist[i])
    kf_list = np.log(kf_list)
    Kc_list = np.log(Kc_list)

    #Get fitting parameters from least squares optimisation fitting
    kf_fit_params, kf_cond_number = lso(Tlist, kf_list, kf_phi)
    Kc_fit_params, Kc_cond_number = lso(Tlist, Kc_list, Kc_phi)

    #Apply appropriate conversion to fitting parameters, and assign to reaction class properties
    reaction.forwardReactionParams.A = np.exp(kf_fit_params[0])
    reaction.forwardReactionParams.n = kf_fit_params[1]
    reaction.forwardReactionParams.E = kf_fit_params[2]

    reaction.reverseReactionParams.G1 = Kc_fit_params[0]
    reaction.reverseReactionParams.G2 = Kc_fit_params[1]
    reaction.reverseReactionParams.G3 = Kc_fit_params[2]
    reaction.reverseReactionParams.G4 = Kc_fit_params[3]
    reaction.reverseReactionParams.G5 = Kc_fit_params[4]





    

