"""
    Author: Brad Semple
    Date: 2014-ish

    Contains the method to apply a least squares optimisation fit for some set of data points
    and a set of basis functions.

    lso function takes a set of x and y points, as well as a list of basis functions in phi
    Returns a list of fitting coefficients to scale the strength of each basis function, 
    and a condition number to show the susceptibility to small changes in the data set.
"""

import numpy as np
def lso(xlist,ylist,phi):
    #Lets say our function takes on the form Ax=b
    A = np.zeros((len(phi),len(phi))) #Make our A matrix an MxM
    b= np.zeros((len(phi),1)) #Make our b vector M long
    
    def matsum(n,m,phi,xlist): #The function that calculates each matrix entry
        funcval=0.0 #initially set the matrix value to zero
        for a in xlist:
            funcval+=phi[m](a)*phi[n](a) #Then add on progressive products to complete the sum
        return funcval
    
    def bsum(n,xlist,ylist): #The function to calculate the values in our 'b' vector
        val=0.0 #do a similar thing as with out other function to calculate the sum
        for a in range(0,len(xlist)):
            val+=ylist[a]*phi[n](xlist[a])
        return val
    
    for N in range(0,len(phi)):
        for M in range(0,len(phi)):
            A[N,M] = matsum(N,M,phi,xlist)
            
    for N in range(0,len(phi)):
        b[(N,0)]=bsum(N,xlist,ylist)
    
    
    alpha=np.linalg.solve(A,b)
    con=np.linalg.cond(A)
    return alpha,con