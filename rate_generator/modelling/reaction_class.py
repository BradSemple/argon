"""
    Contains the custom class for a reaction between two energy levels
"""
import modelling.electronic_rate_model_annaloro as electronic_rate_model
import modelling.heavy_rate_model_annaloro as heavy_rate_model
import fitting.curve_fitting
import pandas as pd

class Reaction:
    def __init__(self, lowerLevel, upperLevel, collisionType):
        self.i = lowerLevel
        self.j = upperLevel
        self.lowerData = pd.Series()
        self.upperData = pd.Series()
        self.collisionType = collisionType #'e' for electron collision, 'A' for heavy particle collision
        self.transitionType = str()
        self.forwardReactionParams = pd.Series(dict.fromkeys(['A', 'n', 'E']))
        self.reverseReactionParams = pd.Series(dict.fromkeys(['G1', 'G2', 'G3', 'G4', 'G5']))
    
    def fill_data(self, dataframe):
        """
            input dataframe has the level index as the dataframe index.
        """
        for key in dataframe.loc[self.i].keys():
            self.lowerData[key] = dataframe.loc[self.i][key]
        for key in dataframe.loc[self.j].keys():
            self.upperData[key] = dataframe.loc[self.j][key]
        self.transitionType = self.get_transition_type()

        
    def get_transition_type(self):
        if self.upperData['term'] == 'i':
            return 'ion'
        if self.lowerData['term'] == self.upperData['term']:
            return 'P'
        if (self.lowerData['J'] == 0 and self.upperData['J'] == 0):
            return 'S'
        if (abs(self.lowerData['J'] - self.upperData['J']) <= 1):
            return 'A'
        return 'S'

    def get_rate_coef(self, T):
        """
            Supplies 'self' and temperature to the rate model file
            returns forward and reverse reaction rate
        """
        if self.collisionType == 'e':
            return electronic_rate_model.get_rate_coef(self, T)
        elif self.collisionType == 'A':
            return heavy_rate_model.get_rate_coef(self, T)
        else: print("Unrecognised collision type for rate model")
    
    def get_fit_parameters(self):
        """
            Populates entries for the fitting parameters for the forward rate coefficient and the equilibirum coefficient
        """
        fitting.curve_fitting.get_fit_parameters(self)
    


    

