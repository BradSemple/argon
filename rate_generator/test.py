import pandas as pd
import numpy as np
from modelling.reaction_class import Reaction
import matplotlib.pyplot as plt

data = pd.read_csv('../data/levels.csv')
data.set_index('index', inplace=True)

react = Reaction(19, 23)
react.fill_data(data)

Tlist = np.linspace(2000, 30000, 100)
k = np.zeros(100)

for i, T in enumerate(Tlist):
    k[i] = react.get_rate_coef(T)

plt.plot(Tlist, k, label = "Transition 19 - 23")
plt.yscale('log')
plt.legend()
plt.show()
