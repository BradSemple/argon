import pandas as pd
import numpy as np
from modelling.reaction_class import Reaction
import matplotlib.pyplot as plt

data = pd.read_csv('../data/levels.csv')
data.set_index('index', inplace=True)


reaction_list = list()
for j in range(2, 6):
    reaction_list.append(Reaction(1, j))
for react in reaction_list:
    react.fill_data(data)

Tlist = np.linspace(1, 10, 30)
k2 = np.zeros(30)
k3 = np.zeros(30)
k4 = np.zeros(30)
k5 = np.zeros(30)


for i in range(len(Tlist)):
    k2[i] = 1e6*reaction_list[0].get_rate_coef(Tlist[i]*11604.5)[0]
    k3[i] = 1e6*reaction_list[1].get_rate_coef(Tlist[i]*11604.5)[0]
    k4[i] = 1e6*reaction_list[2].get_rate_coef(Tlist[i]*11604.5)[0]
    k5[i] = 1e6*reaction_list[3].get_rate_coef(Tlist[i]*11604.5)[0]

k_avg = (k2 + k3 + k4 + k5)/4.0

plt.plot(Tlist, k2, label='1 - 2 Transition')
plt.plot(Tlist, k3, label='1 - 3 Transition')
plt.plot(Tlist, k4, label='1 - 4 Transition')
plt.plot(Tlist, k5, label='1 - 5 Transition')
plt.plot(Tlist, k_avg, label='3p - 4s Average')
plt.yscale('log')
plt.legend()
plt.show()
